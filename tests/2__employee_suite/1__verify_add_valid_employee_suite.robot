*** Settings ***
Documentation
...

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

*** Test Cases ***
Verify Valid Login Test
    Input Text    name=username    Admin
    Input Password    name=password    admin123
    Click Element    xpath=//button[@type='submit']
    Element Text Should Be    xpath=//h6[contains(normalize-space(),'Dashboard')]    Dashboard
    
    Click Element    xpath=//span[text()='PIM']
    
    Click Element    xpath=//a[contains(text(),'Add Employee')]
    Capture Page Screenshot
    
    Input Text    name=firstName    John
    Input Text    name=middleName    J
    Input Text    name=lastName    Wick
    Input Text    xpath=(//input[@class='oxd-input oxd-input--active'])[2]    4545
    
    #Wait Until Element Is Visible    locator    timeout=10s
    #Click Element    xpath=//i[@class='oxd-icon bi-plus'] xpath=(//img[@class='employee-image'])[1]
    Choose File    xpath=//i[@class='oxd-icon bi-plus']    C:${/}Automation Concepts${/}HybridFramework${/}test_data${/}image.PNG


