*** Settings ***
Documentation   This suite file verifies users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03
...     using Test Template concepts in Settings level

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

Library     DataDriver  file=../../test_data/template_invalid.xls   sheet_name=VerifyInvalidLogin

Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1

*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${expected_error}
    Input Text    name=username    ${username}
    Input Password    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    #Assert "Invalid Credentials"
    Element Should Contain    xpath=//p[contains(normalize-space(),'Invalid')]    ${expected_error}
