*** Settings ***
Documentation   This suite file verifies users are allowed to login to the dashboard
...     and it is connected to test case TC_OH_02
...     using Test Template concepts in Testcase level

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

#Test Template   Verify Valid Login Template

*** Test Cases ***
TC1 Verify Valid Login Test
    [Template]   Verify Valid Login Template
    Admin   admin123    Dashboard

TC2 Verify Valid Login Test
    [Template]   Verify Valid Login Template
    Admin   admin123    Dashboard

TC3
    [Setup]
    Log To Console    Completed task
    [Teardown]


*** Keywords ***
Verify Valid Login Template
    [Arguments]     ${username}     ${password}     ${expected_header}
    Input Text    name=username    ${username}
    Input Password    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    Element Text Should Be    xpath=//h6[contains(normalize-space(),'Dashboard')]    ${expected_header}
