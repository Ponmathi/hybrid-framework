*** Settings ***
Documentation
...

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

*** Test Cases ***
Verify Valid Login Test
    #Launch Browser And Navigate To Url
    Input Text    name=username    Admin
    Input Password    name=password    admin123
    Click Element    xpath=//button[@type='submit']
    #${text}     Get Text    xpath=//h6[contains(normalize-space(),'Dashboard')]
    #Log    ${text}
    Element Text Should Be    xpath=//h6[contains(normalize-space(),'Dashboard')]    Dashboard
    #Close Browser - will not terminate if any line throws error. Instead use [Teardown]
    #...    use this [Teardown] in all testcase use it in the settings
    #[Teardown]  Close Browser

TC1
    # If the browser is not required override [Setup] and [Teardown] in the specific testcase
    [Setup]     None
    Log To Console    Completed all testcase
    [Teardown]  Log To Console    No Browser