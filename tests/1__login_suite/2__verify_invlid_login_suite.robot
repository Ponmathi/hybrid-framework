*** Settings ***
Documentation   This suite file verifies users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

*** Test Cases ***
Verify Valid Login Test
    Input Text    name=username    test
    Input Password    name=password    admin123
    Click Element    xpath=//button[normalize-space()='Login']
    #Assert "Invalid Credentials"
    Element Should Contain    xpath=//p[contains(normalize-space(),'Invalid')]    Invalid credentials


