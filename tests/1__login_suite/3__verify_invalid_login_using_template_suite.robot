*** Settings ***
Documentation   This suite file verifies users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03
...     using Test Template concepts in Settings level

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown      Close Browser

Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1
    John    John123     Invalid credentials
TC2
    Pon    Pon123     Invalid credentials
TC3
    ${EMPTY}    Test123     Required
TC4
    par    ${EMPTY}     Required


*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${expected_error}
    Input Text    name=username    ${username}
    Input Password    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    IF  '${username}' == '${EMPTY}'
        #Assert username is empty
        Element Should Contain    xpath=//input[@name='username']/../../span    ${expected_error}
    ELSE IF     '${password}' == '${EMPTY}'
        #Assert Password is empty
        Element Should Contain    xpath=//input[@name='password']/../../span    ${expected_error}
    ELSE
        #Assert "Invalid Credentials"
        Element Should Contain    xpath=//p[contains(normalize-space(),'Invalid')]    ${expected_error}
    END
